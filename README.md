# 24_hours_market_trade_statistics

Pycharm

author: Diao Jinlong
server: dev02 47.45.50.69

exchange: kucoin, okex, hadax, coinex, coinsuper
# all the exchanges above are avilable

function:
for market data:
	Get the k_line data of the exchanges by Api(public market data), return the last 24 hours market trade volume for the currecy pairs, named Market_time_''.xls.
	Then divide the last 24 hours trade price into 10 intervals and sum it, return Market_price_''.xls
for strategy data:
	Get data from database, then return the similar results as the market one, you can change the strategy_name for your own requirements.

Send email every hour.
